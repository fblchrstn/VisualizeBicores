
#include <Arduino.h>

const int NUM_BICORES = 3;

float activity[NUM_BICORES];
float low_pass[NUM_BICORES];
float tau=100;
void setup()
{
  Serial.begin(115200);
  for (int i = 0; i < NUM_BICORES; i++)
  {
    activity[i] = 0;
  }
}

void loop()
{
  for (int i = 0; i < NUM_BICORES; i++)
  {
    activity[i] = analogRead(i);
  }
  for (int i = 0; i < NUM_BICORES; i++)
  {
    low_pass[i] += 1 / tau * (-low_pass[i] + activity[i]);
    Serial.print(low_pass[i]);
    if (i < NUM_BICORES - 1) {
      Serial.print(",");
    }
  }
  Serial.println();
}
