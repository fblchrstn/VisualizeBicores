class drawTile
{
  int index_horizontal;
  int index_vertical;
  int left_corner=0;
  int upper_corner=0;
  int local_width=100;
  int local_height=100;
  int draw_buffer_size=2000;
  float [] x_coord_buffer;
  float [] y_coord_buffer;
  //conctructor
  drawTile(int index_horizontal, int index_vertical, int left_corner, int upper_corner, int local_width, int local_height, int draw_buffer_size)
  {
    this.index_horizontal=index_horizontal;
    this.index_vertical=index_vertical;
    this.left_corner=left_corner;
    this.upper_corner=upper_corner;
    this.local_width=local_width;
    this.local_height=local_height;
    this.draw_buffer_size=draw_buffer_size;
    x_coord_buffer= new float[draw_buffer_size];
    y_coord_buffer= new float[draw_buffer_size];
  }
  void draw()
  {
    int which = frameCount % draw_buffer_size;
    if (mappedData!=null)
    {
      x_coord_buffer[which] =(this.local_width/(0.3*width))* mappedData[this.index_horizontal];
      y_coord_buffer[which] =(this.local_height/(0.6*height))* mappedData[this.index_vertical];
    }
    noFill();
    //    rect(this.left_corner, this.upper_corner, this.local_width, this.local_height);
    for (int i=0; i<draw_buffer_size-1; i++)
    {
      stroke(0,40);
     
      strokeWeight(2);
      int index = (which+1 + i) % draw_buffer_size;
       stroke(0,10+150.0*index/(1.0*draw_buffer_size));
      if (index==0)
      {
        index++;
      }
      line(this.left_corner+x_coord_buffer[index-1], this.upper_corner+y_coord_buffer[index-1], this.left_corner+x_coord_buffer[index], this.upper_corner+y_coord_buffer[index]);
      
    }
    stroke(255,0,0,180);
    fill(255,0,0,180);
    ellipse(this.left_corner+x_coord_buffer[which],this.upper_corner+y_coord_buffer[which],15,15);
  }
}
