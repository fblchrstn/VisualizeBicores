import processing.serial.*;
import processing.pdf.*;
boolean record=false;

int pseudoSerialCounter=0;
float [] mappedData;

float phase_x=0, old_phase_x, phase_y=0, old_phase_y, phase_z=0, old_phase_z;

Serial myPort;
drawTile tile1;
drawTile tile2;
drawTile tile3;
int drawBufferSize = 13330;

float mx[] = new float[drawBufferSize];
float my[] = new float[drawBufferSize];
float mz[] =new float[drawBufferSize];

void setup() {
  size(1250, 700);
  println(Serial.list());
  // I know that the first port in the serial list on my Mac is always my
  // Arduino, so I open Serial.list()[0].
  // Open whatever port is the one you're using.
  myPort = new Serial(this, Serial.list()[0], 115200);
  // don't generate a serialEvent() unless you get a newline character:
  //myPort.bufferUntil('\n');
  background(255);
  tile1=new drawTile(0, 1, 50, 200, 400, 400, 1300);
  tile2=new drawTile(0, 2, 400,200, 400, 400, 1300);
  tile3=new drawTile(1, 2, 800, 200, 400, 400, 1300);
  //beginRecord(PDF, "everything.pdf");
}

void draw() {
  background(255, 1);
  tile1.draw();
  tile2.draw();
  tile3.draw();
}

void serialEvent(Serial myPort) {
  try {
    // get the ASCII string:
    String inString = myPort.readStringUntil('\n');
    if (inString != null) {
      // trim off any whitespace:
      inString = trim(inString);
      // split the string on the commas and convert the resulting substrings
      // into an integer array:
      float[] data = float(split(inString, ","));
      if (data!=null)
      {
        if (mappedData==null)
          mappedData= new float[data.length];
        if (data.length>0)
          for (int i=0; i<data.length; i++)
          {
            if (data[i]==data[i])
              mappedData[i]=map(data[i], 0, 1023, 0, height);
          }
      }
    }
  }
  catch(RuntimeException e) {
    e.printStackTrace();
  }
}
void mousePressed() {
  //beginRecord(PDF, "Lines.pdf");
  background(255);
}

void mouseReleased() {
  //endRecord();
  background(255);
}

void keyPressed()
{
  if (key=='p')
  {
    background(255);
    beginRecord(PDF, "Pattern.pdf");
  }
  if (key=='o')
  {
    endRecord();
  }
}
void exit() {
    println("Exiting program");
    myPort.stop();  // stoping serial port.
   //your Code above. 
   super.exit();
}
